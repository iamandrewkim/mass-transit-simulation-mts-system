import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

public class MainWindow extends JFrame {
    //region Java GUI Elements Region
    private JTable BusDataTable;
    private JPanel TopPanel;
    private JPanel SystemDataPanel;
    private JPanel MiscDataPanel;
    private JPanel BusStopBusEditPanel;
    private JPanel BusDataPanel;
    private JPanel SimulatorControlPanel;
    private JPanel MainLayoutPanel;
    private JPanel BusStopDataPanel;
    private JPanel EditOptionsPanel;
    private JLabel TopPanelLabel;
    private JTextField SystemTimeTextbox;
    private JLabel SystemTimeLabel;
    private JLabel SystemDataLabel;
    private JLabel PassWaitingLabel;
    private JLabel NumberRouteLabel;
    private JLabel NumberStopLabel;
    private JLabel NumberBusLabel;
    private JTextField TotalPassWaitingTextbox;
    private JTextField TotalNumberRouteTextbox;
    private JTextField TotalNumberStopsTextbox;
    private JTextField TotalNumberBusesTextbox;
    private JLabel SimulatorControlLabel;
    private JButton MoveBusButton;
    private JButton ResetBusButton;
    private JButton StopStartoverButton;
    private JButton RewindButton;
    private JComboBox EventIDCombobox;
    private JLabel EventIDLabel;
    private JLabel BusDataLabel;
    private JScrollPane BusDataTableScrollPane;
    private JLabel BusStopDataLabel;
    private JScrollPane BusStopDataScrollPane;
    private JTable BusStopDataTable;
    private JLabel EditBusLabel;
    private JLabel BusIDLabel;
    private JLabel RouteIDLabel;
    private JLabel PassCapacityLabel;
    private JLabel SpeedLabel;
    private JTextField PassCapacityTextbox;
    private JTextField SpeedTextbox;
    private JComboBox BusIDCombobox;
    private JComboBox RouteIDCombobox;
    private JButton EditBusUpdateButton;
    private JLabel EditSysEffLabel;
    private JButton UpdateKButton;
    private JLabel KSpeedLabel;
    private JLabel KCapacityLabel;
    private JLabel KWaitingLabel;
    private JTextField KCapacityTextbox;
    private JTextField KWaitingTextbox;
    private JTextField KSpeedTextbox;
    private JLabel KBusesLabel;
    private JLabel KCombinedLabel;
    private JTextField KBusesTextbox;
    private JTextField KCombinedTextbox;
    private JLabel SysEffLabel;
    private JTextField SystemEfficiencyTextbox;
    private JLabel StartingStopLabel;
    private JComboBox StartingStopIDCombobox;
    private JScrollPane RouteDataScrollPane;
    private JLabel RouteDataLabel;
    private JTable RouteDataTable;
    private JLabel ChangeCompleteLabel;
    private JTextField ConsoleOutTextbox;
    private JLabel ConsoleOutLabel;
    //endregion
    private MTS_System CurrentSystem;
    private Integer EditedBusID = -1;
    private Boolean UpdatingBusCombobox = false;
    private String ChangeDefaultString = " is Complete, Changes will be Reflected on the Next Move Bus Click. ";

    MainWindow(MTS_System system) {

        // set class system
        CurrentSystem = system;

        // set title
        setTitle("Assignment 9 - Group 9-81");

        // set size
        Dimension screenSize = new Dimension(1280,1024);
        setSize(screenSize);
        setPreferredSize(screenSize);
        setMinimumSize(screenSize);

        // add main jframe
        add(MainLayoutPanel);

        // quit application when window is closed
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // load all initial data into GUI
        RefreshAll();
        ToggleChangeIndicator(false, "");
        ToggleBusEditPanel(false);

        // set enabled/visible
        EditBusUpdateButton.setEnabled(false);

        // ----------- click events below -----------

        // move bus event
        MoveBusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CurrentSystem.passNewEvent("move_bus");
                ToggleBusEditPanel(false);
                ToggleChangeIndicator(false, "");
                RefreshAll();
            }
        });

        // reset bus event
        ResetBusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CurrentSystem.reset_buses();
                ToggleChangeIndicator(false, "");
                RefreshAll();
            }
        });

        // replay event
        RewindButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CurrentSystem.rewind( Integer.parseInt(EventIDCombobox.getSelectedItem().toString()));
                ToggleChangeIndicator(true, "Rewind");
                RefreshSimulatorControlPanel();
            }
        });

        // update bus event
        EditBusUpdateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ToggleChangeIndicator(true, "Bus Change");
                String editedBusID = BusIDCombobox.getSelectedItem().toString();
                String editedBusRoute = RouteIDCombobox.getSelectedItem().toString();
                String editedBusStartingStop = StartingStopIDCombobox.getSelectedItem().toString();
                String editedBusSpeed = SpeedTextbox.getText();
                String editedBusCapacity = PassCapacityTextbox.getText();
                CurrentSystem.passNewEvent("change_bus," + editedBusID + "," + editedBusRoute + "," + editedBusStartingStop + "," + editedBusSpeed + "," + editedBusCapacity);
            }
        });

        // update K constants
        UpdateKButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Double kSpeed, kCapacity, kBuses, kWaiting, kCombined;
                // get data from text boxes
                kSpeed = Double.parseDouble(KSpeedTextbox.getText());
                kCapacity = Double.parseDouble(KCapacityTextbox.getText());
                kBuses = Double.parseDouble(KBusesTextbox.getText());
                kWaiting = Double.parseDouble(KWaitingTextbox.getText());
                kCombined = Double.parseDouble(KCombinedTextbox.getText());
                // update coefficients
                CurrentSystem.changeCoefficients(kSpeed, kCapacity, kBuses, kWaiting, kCombined);
                // refresh GUI
                RefreshSystemEfficiencyPanel();

            }
        });

        // bus id combobox changed
        BusIDCombobox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(UpdatingBusCombobox) return;
                EditedBusID = Integer.parseInt(BusIDCombobox.getSelectedItem().toString());
                ToggleBusEditPanel(true);
                UpdatingBusCombobox = true;
                RouteIDCombobox.removeAllItems();
                RefreshBusEditPanel();
                EditBusUpdateButton.setEnabled(true);
                UpdatingBusCombobox = false;
            }
        });

        // route combobox changed
        RouteIDCombobox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {

                if(UpdatingBusCombobox) return;
                Bus selectedBus = CurrentSystem.getBusByID(Integer.parseInt(BusIDCombobox.getSelectedItem().toString()));
                // load new stops into combobox
                StartingStopIDCombobox.removeAllItems();
                Route selectedRoute = CurrentSystem.getRouteByID(Integer.parseInt(RouteIDCombobox.getSelectedItem().toString()));
                ArrayList<Stop> stops = selectedRoute.getMyStops();
                for (Stop stop : stops) {
                    StartingStopIDCombobox.addItem(stop.getStopID());
                }

            }
        });
    }

    // refresh top panel data
    public void RefreshTopPanel() {

        // update system time
        SystemTimeTextbox.setText(CurrentSystem.getSystemTime().toString());

    }

    // refresh system data panel data
    public void RefreshSystemDataPanel() {

        // total number of passengers waiting
        TotalPassWaitingTextbox.setText(Double.toString(CurrentSystem.getTotalPassengersWaiting()));
        // total number of routes
        TotalNumberRouteTextbox.setText(Integer.toString(CurrentSystem.getActiveRoutes().size()));
        // total number of bus stops
        TotalNumberStopsTextbox.setText(Integer.toString(CurrentSystem.getActiveStops().size()));
        // total number of buses
        TotalNumberBusesTextbox.setText(Integer.toString(CurrentSystem.getActiveBuses().size()));

    }

    // refresh simulator control panel data
    public void RefreshSimulatorControlPanel() {


        // clear combobox
        EventIDCombobox.removeAllItems();
        // if no events left to rewind, disable combobox and button
        if(CurrentSystem.getMaxNumberOfEventsToRewind() == 0){
            EventIDCombobox.addItem("No events to rewind");
            EventIDCombobox.setEnabled(false);
            RewindButton.setEnabled(false);
        } else {
            EventIDCombobox.setEnabled(true);
            RewindButton.setEnabled(true);
            for(int i = 1; i <= CurrentSystem.getMaxNumberOfEventsToRewind(); i++){
                EventIDCombobox.addItem(i);
            }
        }

        ConsoleOutTextbox.setText(CurrentSystem.getConsoleOutString());
    }

    // refresh bus data panel data
    public void RefreshBusDataPanel() {

        // clear existing buses in combobox
        UpdatingBusCombobox = true;
        BusIDCombobox.removeAllItems();
        // set column names
        String[] columnNames = {"Bus ID", "Passenger Count", "Current Stop", "Next Stop", "Time to Next Stop", "Speed", "Initial Capacity", "Current Route"};
        DefaultTableModel busTableModel = new DefaultTableModel();
        busTableModel.setColumnIdentifiers(columnNames);
        BusDataTable.setModel(busTableModel);
        ArrayList<Bus> buses = CurrentSystem.getActiveBuses();
        Object[] rowData = new Object[8];
        // populate table rows
        for(int i=0; i < buses.size(); i++) {
            rowData[0] = buses.get(i).getBusID();
            rowData[1] = buses.get(i).getPassengersOnBus();
            rowData[2] = buses.get(i).getCurrentStop().getStopID();
            rowData[3] = buses.get(i).getNextStop().getStopID();
            rowData[4] = buses.get(i).getTimeToNextStop();
            rowData[5] = buses.get(i).getSpeedOfBus();
            rowData[6] = buses.get(i).getInitialPassengerCapacity();
            rowData[7] = buses.get(i).getMyRoute().getRouteID();
            BusIDCombobox.addItem(buses.get(i).getBusID());
            EditedBusID = -1;
            busTableModel.addRow(rowData);
        }
        // set combobox selected item to nothing to force event when user changes
        BusIDCombobox.setSelectedIndex(-1);
        UpdatingBusCombobox = false;

    }

    // refresh bus stop panel data
    public void RefreshBusStopPanel() {

        String[] columnNames = {"Stop ID", "Latitude", "Longitude", "Pass. Waiting", "Assigned Routes"};
        DefaultTableModel busStopDataModel = new DefaultTableModel();
        busStopDataModel.setColumnIdentifiers(columnNames);
        BusStopDataTable.setModel(busStopDataModel);
        ArrayList<Stop> stops = CurrentSystem.getActiveStops();
        Object rowData[] = new Object[5];
        // populate table rows
        for(int i=0; i < stops.size(); i++) {
            rowData[0] = stops.get(i).getStopID();
            rowData[1] = stops.get(i).getLatitude();
            rowData[2] = stops.get(i).getLongitude();
            rowData[3] = stops.get(i).getWaiting();
            rowData[4] = CurrentSystem.getRoutesOfStop(stops.get(i).getStopID());
            busStopDataModel.addRow(rowData);
        }
    }

    // refresh bus route panel data
    public void RefreshBusRoutePanel() {

        String[] columnNames = {"Route ID", "Number", "Name", "Assigned Stops", "Assigned Buses"};
        DefaultTableModel busRouteModel = new DefaultTableModel();
        busRouteModel.setColumnIdentifiers(columnNames);
        RouteDataTable.setModel(busRouteModel);
        ArrayList<Route> routes = CurrentSystem.getActiveRoutes();
        Object rowData[] = new Object[5];
        // populate table rows
        for(int i=0; i < routes.size(); i++) {
            rowData[0] = routes.get(i).getRouteID();
            rowData[1] = routes.get(i).getRouteNumber();
            rowData[2] = routes.get(i).getRouteName();
            rowData[3] = CurrentSystem.getStopsOfRoute(routes.get(i).getRouteID());
            rowData[4] = CurrentSystem.getBusesOnRoute(routes.get(i).getRouteID());
            busRouteModel.addRow(rowData);
        }
    }

    // refresh bus edit panel data
    public void RefreshBusEditPanel() {

        // clear existing data
        RouteIDCombobox.setEnabled(true);
        RouteIDCombobox.removeAllItems();
        StartingStopIDCombobox.setEnabled(true);
        StartingStopIDCombobox.removeAllItems();
        if(EditedBusID >= 0) {
            // get selected bus
            Bus selectedBus = CurrentSystem.getBusByID(EditedBusID);
            // populate route combobox with all available routes, select route that bus has now
            ArrayList<Route> routes = CurrentSystem.getActiveRoutes();
            for (Route route : routes) {
                RouteIDCombobox.addItem(route.getRouteID());
                // if selected bus route == current route, set combobox selected
                if(selectedBus.getMyRoute().getRouteID() == route.getRouteID()){
                    RouteIDCombobox.setSelectedItem(route.getRouteID());
                }
            }
            // populate stop combobox with available stops from current route, set combo selected to current stop
            ArrayList<Stop> stops = selectedBus.getMyRoute().getMyStops();
            for (Stop stop : stops) {
                StartingStopIDCombobox.addItem(stop.getStopID());
                // if selected bus current stop == current stop, set combobox selected
                if(selectedBus.getCurrentStop().getStopID() == stop.getStopID()){
                    StartingStopIDCombobox.setSelectedItem(stop.getStopID());
                }
            }
            PassCapacityTextbox.setText(Integer.toString(selectedBus.getInitialPassengerCapacity()));
            SpeedTextbox.setText(Integer.toString(selectedBus.getSpeedOfBus()));

        }
    }

    // refresh system efficiency panel
    public void RefreshSystemEfficiencyPanel(){

        // total system efficiency
        SystemEfficiencyTextbox.setText(Double.toString(CurrentSystem.getSystemEfficiency()));
        // current K values
        KSpeedTextbox.setText(Double.toString(CurrentSystem.getEfficiencyHelper().getK_SPEED()));
        KCapacityTextbox.setText(Double.toString(CurrentSystem.getEfficiencyHelper().getK_CAPACITY()));
        KWaitingTextbox.setText(Double.toString(CurrentSystem.getEfficiencyHelper().getK_WAITING()));
        KBusesTextbox.setText(Double.toString(CurrentSystem.getEfficiencyHelper().getK_BUSES()));
        KCombinedTextbox.setText(Double.toString(CurrentSystem.getEfficiencyHelper().getK_COMBINED()));

    }

    // refresh all
    public void RefreshAll() {
        RefreshTopPanel();
        RefreshSystemDataPanel();
        RefreshSimulatorControlPanel();
        RefreshBusDataPanel();
        RefreshBusRoutePanel();
        RefreshBusStopPanel();
        RefreshSystemEfficiencyPanel();
    }

    public void ToggleBusEditPanel(Boolean toggleChoice) {
        // toggleChoice = true, enable bus edit panel controls
        // toggleChoice = false, disable bus edit panel controls
        RouteIDCombobox.setEnabled(toggleChoice);
        StartingStopIDCombobox.setEnabled(toggleChoice);
        PassCapacityTextbox.setEnabled(toggleChoice);
        SpeedTextbox.setEnabled(toggleChoice);
        EditBusUpdateButton.setEnabled(toggleChoice);
        // clear data if not being used
        if(!toggleChoice) {
            UpdatingBusCombobox = true;
            RouteIDCombobox.removeAllItems();
            StartingStopIDCombobox.removeAllItems();
            PassCapacityTextbox.setText("");
            SpeedTextbox.setText("");
            UpdatingBusCombobox = false;
        }
    }

    public void ToggleChangeIndicator(Boolean choice, String indication) {

        ChangeCompleteLabel.setText(indication + ChangeDefaultString);
        ChangeCompleteLabel.setVisible(choice);
    }
}
