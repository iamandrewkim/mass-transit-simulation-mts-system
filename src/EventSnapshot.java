class EventSnapshot {
    Event ProcessedEvent;
    Bus ActiveBus;

    EventSnapshot(Event processedEvent, Bus activeBus) {
        ProcessedEvent = processedEvent;
        ActiveBus = activeBus;
    }
}
