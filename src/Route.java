

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;

public class Route<T> {
    private Integer routeID;
    private Integer routeNumber;
    private String routeName;
    private ArrayList<Stop> myStops = new ArrayList<Stop>();

    public Route(String[] inputCmds) {
        //System.out.println(" +++ Creating Route with ID " + inputCmds[1]);
        // Route input cmd == ID, initial stop, name
        this.routeID = Integer.parseInt(inputCmds[1]);
        this.routeNumber = Integer.parseInt(inputCmds[2]);
        this.routeName = inputCmds[3];

    }

    public Route(Route routeToClone) {
        //System.out.println(" +++ Cloning Route with ID " + routeToClone.routeID);
        this.routeID = routeToClone.routeID;
        this.routeNumber = routeToClone.routeNumber;
        this.routeName = routeToClone.routeName;
        this.myStops.addAll(routeToClone.myStops);
    }


    public void extend(Stop newStop){
        //System.out.println(" +++ Extending Route " + this.routeName + " to include stop " + newStop.getStopID());
        this.myStops.add(newStop);
    }

    public String getRouteName(){
        return String.valueOf(this.routeID);
    }


    public ArrayList<Stop> getStops(){
        ArrayList<Stop> s = new ArrayList<>();
        Iterator<Stop> itr = this.myStops.iterator();
        while(itr.hasNext()){
            s.add((itr.next()));
        }
        return s;
    }

    public Integer getNumberOfStops(){
        return this.myStops.size();
    }

    public Stop getNextStop( Integer currentIndex ){

        Stop myStop = this.myStops.get(currentIndex);

        if (currentIndex == this.myStops.size() - 1){
            return this.myStops.get(0);
        }
        else{
            return this.myStops.get(currentIndex + 1);

        }
    }

    public Integer getStopIndexWithID(Integer stopid){
        Iterator<Stop> itr = this.myStops.iterator();
        Stop s = null;
        Integer r = 0;
        while(itr.hasNext()){
            s = itr.next();
            if(s.getStopID() == stopid){
                return r;
            }
            r++;
        }
        return r;
    }

    public Stop getStopAtIndex(String index){
        return this.myStops.get(Integer.parseInt(index));
    }

    public Integer getRouteID(){return this.routeID;}
    public ArrayList<Stop> getMyStops(){return this.myStops;}
    public Integer getRouteNumber(){return this.routeNumber;}

}
