

public class SystemEfficiencyHelper {
    private Double K_SPEED;
    private Double K_CAPACITY;
    private Double K_WAITING;
    private Double K_COMBINED;
    private Double K_BUSES;

    public SystemEfficiencyHelper(){
        this.K_SPEED = 1.0;
        this.K_CAPACITY = 1.0;
        this.K_WAITING = 1.0;
        this.K_COMBINED = 1.0;
        this.K_BUSES = 1.0;
    }

    public void setK_BUSES(Double k_BUSES) {
        this.K_BUSES = k_BUSES;
    }

    public void setK_CAPACITY(Double k_CAPACITY) {
        this.K_CAPACITY = k_CAPACITY;
    }

    public void setK_COMBINED(Double k_COMBINED) {
        this.K_COMBINED = k_COMBINED;
    }

    public void setK_SPEED(Double k_SPEED) {
        this.K_SPEED = k_SPEED;
    }

    public void setK_WAITING(Double k_WAITING) {
        this.K_WAITING = k_WAITING;
    }

    public Double getK_BUSES() {
        return this.K_BUSES;
    }

    public Double getK_CAPACITY() {
        return this.K_CAPACITY;
    }

    public Double getK_COMBINED() {
        return this.K_COMBINED;
    }

    public Double getK_SPEED() {
        return this.K_SPEED;
    }

    public Double getK_WAITING() {
        return this.K_WAITING;
    }

}
